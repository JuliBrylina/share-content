## About extention

It is a browser extension that allow sharing any content on the page with anyone using their email/phone number.
1. OTP authentication with email or phone number - try to use firebase firestore; it has everything you need for auth/otp. And you can use firebase for everything else as well. It allows to build concepts very quickly and scale;
2. Right click on any highlighted text/image: share with _______(phone/email);
