/*
 * bg.js -- a ManifestV3 service_worker that installs a context menu
 *          plus minimal framework for messaging between here and
 *          a app.js script.
 */

console.log('BG context menu clicked');

chrome.runtime.onInstalled.addListener(function () {
    chrome.contextMenus.create({
        id: 'my-context-item',
        title: "Share '%s' text by email",
        contexts: ['selection']
    });
    chrome.contextMenus.create({
        id: 'my-context-item-2',
        title: "Share '%s' text by phone",
        contexts: ['selection']
    });
});

searcGoogle = function(info) {

    let query = info.selectionText;
    console.log(query + '- word share');
    //chrome.tabs.create({ url: "https://www.google.com/search?q=" + query });
};

chrome.contextMenus.onClicked.addListener(function (info, tabs) {
    searcGoogle(info);
    console.log('context menu clicked');
    console.log(info);
    console.log(tabs);
    chrome.tabs.sendMessage(tabs.id, 'request-object',
        (rsp) => {
            console.log("content script replies:");
            console.log(rsp);
        });
});